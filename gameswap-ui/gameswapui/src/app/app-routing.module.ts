import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,RouterModule} from "@angular/router";
import {ProfilesComponent} from "./profiles/profiles.component";
import {ProfileCreateComponent} from "./profile-create/profile-create.component";
import {UserDetailComponent} from "./user-detail/user-detail.component";
import {HomeComponent} from "./home/home.component";
import { StoreComponent } from './store/store.component';

const routes : Routes = [
  { path: 'home', component:HomeComponent },
  { path: '', redirectTo:'home', pathMatch:'full'},
  { path: 'profile-create' , component: ProfileCreateComponent },
  { path: 'profiles' , component:ProfilesComponent },
  { path: 'user-detail', component: UserDetailComponent},
  { path: 'store', component: StoreComponent}
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
