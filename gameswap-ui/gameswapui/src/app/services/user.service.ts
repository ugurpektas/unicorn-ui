import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/user";

@Injectable()

export class UserService {

  url = "http://localhost:3000/users";

  constructor(private http: HttpClient) { }

  //http - get
  getUser(): Observable<User[]> {
    return  this.http.get<User[]>(this.url);
  }

  //http - post
  createUser(user:User): Observable<User> {
    const httpOptions = {
      headers : new HttpHeaders({
        'content-type' : 'application/json',
        'Authorization' : 'Token'
      })
    }
    return this.http.post<User>(this.url,user,httpOptions);
  }
}
