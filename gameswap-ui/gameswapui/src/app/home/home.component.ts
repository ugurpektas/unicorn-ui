import { Component, OnInit } from '@angular/core';
import { User } from "../models/user";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService]
})
export class HomeComponent implements OnInit {

  users : User[] = [];

  constructor(
    private userService:UserService,
    private http: HttpClient,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe(data => {
      this.users = data;
      console.log(data);
    })
  }

  loginForm = new FormGroup({
    name: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(12)]),
    password: new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(12)]),
  });

  login(data:any) {
    console.log(data);
  }

}
