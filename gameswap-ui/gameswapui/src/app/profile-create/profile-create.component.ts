import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile-create',
  templateUrl: './profile-create.component.html',
  styleUrls: ['./profile-create.component.css'],
  providers: [UserService]
})
export class ProfileCreateComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  createUserForm = new FormGroup({
    kullaniciAd: new FormControl('',[Validators.required]),
    kullaniciSoyad: new FormControl('',[Validators.required]),
    kullaniciNick: new FormControl('',[Validators.required]),
    kullaniciDT: new FormControl('',[Validators.required]),
    kullaniciSifre: new FormControl('',Validators.required)
  });

  create(data:any) {
    this.userService.createUser(data).subscribe(data=>{
      console.log(data);
      console.log("Kayıt gönderildi");
      this.router.navigate(['/home']);
    });
  }

}
