export interface User{
  id: number;
  kullaniciAd : string;
  kullaniciSoyad: string;
  kullaniciSifre : string;
  kullaniciNick : string;
  kullaniciDT : string;
}
