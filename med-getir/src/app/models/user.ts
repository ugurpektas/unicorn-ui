export interface User{
  id: number;
  name : string;
  surname: string;
  password : string;
  nationality_id : string;
  phone : string;
  birthOfDate : bigint;
}
