import { Component, OnInit } from '@angular/core';
import {User} from "../models/user";
import {EczaneService} from "../services/eczane.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
  providers: [UserService]
})
export class UserCreateComponent implements OnInit {

  user : User[] = [];

  constructor(
    private userService:UserService,
    private http: HttpClient,
    private route: Router
  ) { }

  ngOnInit(): void {
  }

  kayitUser = new FormGroup({
    userName: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(12)]),
    userSurname: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(12)]),
    userPhone: new FormControl('',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]),
    userBirthDay: new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(4)]),
    userTCKN: new FormControl('',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]),
    userPassword: new FormControl('',Validators.required),
    userRepassword: new FormControl('',Validators.required)
  });

  userKayit(data:any) {
    this.userService.createUser(data).subscribe(data => {
      this.route.navigate(['/user']);
      console.log(data);
      console.log("Kayıt gönderildi")
    });
  }

}
