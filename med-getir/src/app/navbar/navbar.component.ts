import { Component, OnInit } from '@angular/core';
import {EczaneService} from "../services/eczane.service";
import {UserService} from "../services/user.service";
import {User} from "../models/user";
import {Eczane} from "../models/eczane";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [EczaneService,UserService]
})
export class NavbarComponent implements OnInit {
  saveForm= true;
  eczane : Eczane[] = [];
  user : User[] = [];

  constructor(
    private eczaneService: EczaneService,
    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.eczaneService.getEczane().subscribe(eczane => {
      this.eczane = eczane;
    },error => console.log(error));

    this.userService.getUser().subscribe(user => {
      this.user = user;
    },error => console.log(error));
  }


}
