import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {User} from "../models/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

  users: User[] = [];

  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe(data => {
      this.users = data;
    })
  }

  girisForm = new FormGroup({
    girisPhone : new FormControl('',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]),
    girisPassword : new FormControl('',Validators.required)
  });

  login() {

  }

}
