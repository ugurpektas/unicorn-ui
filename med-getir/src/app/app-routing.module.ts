import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from "./main/main.component";
import {UserComponent} from "./user/user.component";
import {EczanelerComponent} from "./eczaneler/eczaneler.component";
import {LoginComponent} from "./login/login.component";
import {EczaneUserCreateComponent} from "./eczane-user-create/eczane-user-create.component";
import {UserCreateComponent} from "./user-create/user-create.component";

const routes: Routes = [
  { path: 'main', component:MainComponent },
  { path: '' , redirectTo: 'main' , pathMatch: 'full' },
  { path: 'eczane-kayit', component: EczaneUserCreateComponent },
  { path: 'user-kayit', component: UserCreateComponent },
  { path: 'eczane', component: EczanelerComponent },
  { path: 'login' , component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
