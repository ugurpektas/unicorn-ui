import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EczanelerComponent } from './eczaneler.component';

describe('EczanelerComponent', () => {
  let component: EczanelerComponent;
  let fixture: ComponentFixture<EczanelerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EczanelerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EczanelerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
