import { Component, OnInit } from '@angular/core';
import {Eczane} from "../models/eczane";
import {EczaneService} from "../services/eczane.service";

@Component({
  selector: 'app-eczaneler',
  templateUrl: './eczaneler.component.html',
  styleUrls: ['./eczaneler.component.scss'],
  providers: [EczaneService]
})
export class EczanelerComponent implements OnInit {

  eczane: Eczane[] = [];

  constructor(private eczaneService:EczaneService) { }

  ngOnInit(): void {
    this.eczaneService.getEczane().subscribe(eczane => {
      this.eczane = eczane;
    })
  }

  deletePost(ecz:Eczane) {
    this.eczaneService.deleteEczane(ecz).subscribe(data => {
      console.log(data);
      let index = this.eczane.indexOf(ecz);
      this.eczane.splice(index,1);
    })
  }

}
