import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Eczane} from "../models/eczane";

@Injectable()
export class EczaneService {

  url = "http://localhost:3000/eczane";

  constructor(private http: HttpClient) { }

  getEczane(): Observable<Eczane[]> {
    return  this.http.get<Eczane[]>(this.url);
  }

  createEczane(eczane:Eczane): Observable<Eczane> {
    const httpOptions = {
      headers : new HttpHeaders({
        'content-type' : 'application/json',
        'Authorization' : 'Token'
      })
    }
    return this.http.post<Eczane>(this.url,eczane,httpOptions);
  }

  deleteEczane(post:any) {
    return this.http.delete(this.url+ '/' + post.id);
  }

}
