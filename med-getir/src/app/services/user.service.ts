import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../models/user";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class UserService {

  url = "http://localhost:3000/user";

  constructor(private http: HttpClient) { }

  getUser(): Observable<User[]> {
    return  this.http.get<User[]>(this.url);
  }

  createUser(user:User): Observable<User> {
    const httpOptions = {
      headers : new HttpHeaders({
        'content-type' : 'application/json',
        'Authorization' : 'Token'
      })
    }
    return this.http.post<User>(this.url,user,httpOptions);
  }
}
