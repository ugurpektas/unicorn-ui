import { Component, OnInit } from '@angular/core';
import {EczaneService} from "../services/eczane.service";
import {Eczane} from "../models/eczane";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-eczane-user-create',
  templateUrl: './eczane-user-create.component.html',
  styleUrls: ['./eczane-user-create.component.scss'],
  providers: [EczaneService]
})
export class EczaneUserCreateComponent implements OnInit {

  eczane : Eczane[] = [];

  constructor(
    private eczaneService:EczaneService,
    private http: HttpClient,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.eczaneService.getEczane().subscribe(eczane => {
      this.eczane = eczane;
    },error => console.log(error))
  }

  kayitEczane = new FormGroup({
    name : new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(12)]),
    phone: new FormControl('',[Validators.required,Validators.minLength(11),Validators.maxLength(11)]),
    kategori: new FormControl(''),
    password: new FormControl('',Validators.required),
    repassword: new FormControl('',Validators.required)
  });


  eczaneKayit(data:any) {
    console.log(data);
    this.eczaneService.createEczane(data).subscribe(data => {
      this.route.navigate(['/user']);
      console.log("Kayıt gönderildi")
    })
  }

}
