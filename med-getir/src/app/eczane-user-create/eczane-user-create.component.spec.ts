import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EczaneUserCreateComponent } from './eczane-user-create.component';

describe('EczaneUserCreateComponent', () => {
  let component: EczaneUserCreateComponent;
  let fixture: ComponentFixture<EczaneUserCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EczaneUserCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EczaneUserCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
